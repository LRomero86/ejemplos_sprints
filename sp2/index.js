const express = require('express');
const sequelize = require('./src/database/db');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended:true }));



validar_conexion();

sequelize.sync({ force:true })
    .then(() => {
        console.log('tablas sincronizadas');
    });

async function validar_conexion() {
    try {
        await sequelize.authenticate();
        console.log('Conexión establecida');

        app.listen(3000, () => {
            console.log('listening in port 3000');
        });

    } catch (error) {
        console.error('Error al conectarse a la bd', error);
    }
};