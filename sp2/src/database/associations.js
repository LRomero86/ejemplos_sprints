function associations(sequelize) {
    const { user, addressBook } = sequelize.models;

    user.hasMany(addressBook, { foreignKey: 'user_id' });
    addressBook.belongsTo(user, { foreignKey: 'user_id' });
}

module.exports = { associations };