const { Sequelize } = require('sequelize');
const { database } = require('../config/config');

const userModel = require('../models/user');
const addressModel = require('../models/addressBook');

const { associations } = require('../models/associations');


const sequelize = new Sequelize(
    database.database,
    database.username,
    database.password, {
        host: database.host,
        dialect: 'mysql',
    }
);


const AddressBook = addressModel(sequelize);
const User = userModel(sequelize);
/*
const modelDefiners = [
    addresModel,
    userModel,    
];
*/

associations(sequelize);

module.exports = sequelize;