const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user: DataTypes.STRING(100),
        pass: DataTypes.STRING,
        is_admin: DataTypes.BOOLEAN,
        is_logged: DataTypes.BOOLEAN,
    },
    {
        timestamps: false,
    });

    return User;
};