const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const AddressBook = sequelize.define('AddresBook', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,            
        },
        address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        number: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        otherData: {
            type: DataTypes.STRING(250),
            allowNull: true,
        }
    },
    {
        timestamps: false,
    });
};